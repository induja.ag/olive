
from starlette.config import Config
from starlette.datastructures import Secret
from app.core.settings.settings import BaseConfig


class DevSettings(BaseConfig):

    config = Config()

    DEBUG = config("DEBUG", cast=bool, default=True)
    DB_USER = config("DB_USER", cast=str)
    DB_PASSWORD = config("DB_PASSWORD", cast=Secret)
    DB_HOST = config("DB_HOST", cast=str)
    DB_PORT = config("DB_PORT", cast=str)
    DB_NAME = config("DB_NAME", cast=str)
    DATABASE_URL = config("DATABASE_URL", default=f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}",)  # noqa

