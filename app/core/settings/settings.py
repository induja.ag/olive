
import os
from starlette.config import Config
from starlette.datastructures import CommaSeparatedStrings, Secret

project_name = "olive_auth_services"


class BaseConfig:
    config = Config()
    INCLUDE_SCHEMA = config("INCLUDE_SCHEMA", cast=bool, default=True)
    SECRET_KEY = config("SECRET_KEY", default=os.urandom(32))
    SQLALCHEMY_ECHO = config("SQLALCHEMY_ECHO", cast=bool, default=False)
    SQLALCHEMY_TRACK_MODIFICATIONS = config(
        "SQLALCHEMY_TRACK_MODIFICATIONS", cast=bool, default=False)
    LOGGER_NAME = "%s_log" % project_name
    LOG_FILENAME = "/var/tmp/app.%s.log" % project_name
    CORS_ORIGINS = config("CORS_HOSTS", default="*")
    DEBUG = config("DEBUG", cast=bool, default=True)
    TESTING = config("TESTING", cast=bool, default=False)

    # Authentication
    AUTH_IDENTITY_VERIFY_URL = config(
        "AUTH_IDENTITY_VERIFY_URL", cast=str, default="")
    AUTH_IDENTITY_CLIENT_ID = config(
        "AUTH_IDENTITY_CLIENT_ID", cast=str, default="")
    AUTH_COOKIE_NAME = config(
        "AUTH_COOKIE_NAME", cast=str, default="OAuth.AccessToken.EP")
    AUTH_EXEMPTED_AUTH_ROUTES = config(
        "AUTH_EXEMPTED_AUTH_ROUTES", cast=CommaSeparatedStrings,
        default=(
            "/docs, /openapi.json,"
            "/static/css/styles.css,"
        ))

