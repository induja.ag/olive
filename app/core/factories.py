
import os


envsettings = os.getenv("settings")

if envsettings in ["dev", "default"]:
    from app.core.settings.devsettings import DevSettings
    settings = DevSettings()
else:
    raise SystemExit(
        "settings for app not exported. example:  ```export settings=dev```")

