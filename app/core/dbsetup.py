
from uuid import uuid4
# add created,updated columns to model
from sqlalchemy_utils import UUIDType, Timestamp
from app.core.extensions import db


class SurrogatePK(object):
    __table_args__ = {"extend_existing": True}

    id = db.Column(UUIDType(binary=False), primary_key=True)


class SurrogateAudit(object):

    __table_args__ = {"extend_existing": True}

    _created = db.Column(db.Time(), nullable=True)
    _modified = db.Column(db.Time(), nullable=True)
    _created_by = db.Column(db.String(), nullable=True)
    _modified_by = db.Column(db.String(), nullable=True)


class Model(Timestamp, SurrogatePK, SurrogateAudit, db.Model):
    __abstract__ = True

    @classmethod
    async def create(cls, **kwargs):
        if issubclass(cls, SurrogatePK):
            unique_id = uuid4()
            if not kwargs.get("id"):
                kwargs["id"] = unique_id
        return await cls(**kwargs)._create()

