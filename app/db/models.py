from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from app.api.services.base import BaseModel
from app.db import Base


class Organization(Base):
    __tablename__ = 'organization'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(200))
    size = Column(String(200))
    industy = Column(String(200))


class ProjectWorkspace(Base):
    __tablename__ = 'projectworkspace'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(200))
    organization_id = Column(Integer, ForeignKey('organization.id'))


class Project(Base):
    __tablename__ = 'project'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(200))
    project_workspace_id = Column(Integer, ForeignKey('projectworkspace.id'))


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(200))
    email = Column(String(200), unique=True, index=True)
    first_name = Column(String(200))
    last_name = Column(String(200))


class Group(Base):
    __tablename__ = "group"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(200))


class UserGroups(Base):
    __tablename__ = "usergroups"
    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    group_id = Column(Integer, ForeignKey("group.id"))


class ResourceType(Base):
    __tablename__ = "resourcetype"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(200))


class Role(Base):
    __tablename__ = "role"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(200))


class UserResourceRole(Base, BaseModel):
    __tablename__ = "userresourcerole"
    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("user.id"))
    resource_id = Column(Integer)
    resource_type_id = Column(Integer, ForeignKey("resourcetype.id"))
    role_id = Column(Integer, ForeignKey("role.id"))


class UserGroupResourceRole(Base):
    __tablename__ = "userGroupresourcerole"
    id = Column(Integer, primary_key=True, index=True)
    group_id = Column(Integer, ForeignKey("group.id"))
    resourcetype_id = Column(Integer, ForeignKey("resourcetype.id"))
    resource_id = Column(Integer)
    role_id = Column(Integer, ForeignKey('role.id'))
