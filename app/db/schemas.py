from fastapi import FastAPI
from pydantic import BaseModel


class ResourceRole(BaseModel):
    user_id: int
    role_id: int
    resource_type_id: int
    resource_id: int

    class Config:
        orm_mode = True


class UpdateResourceRole(BaseModel):
    user_id: int
    role_id: int
    resource_type_id: int
    resource_id: int
