import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
formatter = logging.Formatter(
    "%(asctime)s - %(module)s - %(funcName)s - line:%(lineno)d - %("
    "levelname)s - %(message)s "
)
ch.setFormatter(formatter)
logger.addHandler(ch)  # Exporting logs to the screen

logger = logging.getLogger(__name__)
