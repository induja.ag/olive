from app.db import SessionLocal

db = SessionLocal()


class BaseModel:
    def save(self):
        try:
            db.add(self)
            db.commit()
            return self
        except Exception as e:
            db.rollback()
            print(e)

    def delete(self):
        try:
            db.delete(self)
            db.commit()
            return self
        except Exception as e:
            db.rollback()
            print(e)
