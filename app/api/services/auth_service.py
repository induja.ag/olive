from app.db import session, SessionLocal
from app.db import models
from fastapi.responses import JSONResponse

s = SessionLocal()
permission_dict = {
    'organization_owner': {
        'Organization': ['read', 'create', 'delete', 'update'],
        'ProjectWorkspace': ['read', 'create', 'delete', 'update'],
        'Project': ['read', 'create', 'delete', 'update']
    },
    'project_workspace_admin': {
        'ProjectWorkspace': ['read', 'update'],
        'Project': ['read', 'create', 'update', 'delete']
    },
    'project_stakeholder': {
        'Project': ['read', 'update']
    },
    'project_respondent': {
        'Project': ['read', 'update']
    },
    'project_opp_vendor': {
        'Project': ['read']
    }
}


def get_all_resource_type():
    result_set = session.query(models.ResourceType).all()
    result_dict = {result.name: result.id for result in result_set}
    return result_dict


def role_decision(resource_id: int, resourcetype_id: int, user_id: int,
                  operation: str):
    """
    :param resource_id:
    :param resourcetype_id:
    :param user_id:
    :return:
    """
    req_resource = session.query(models.ResourceType).filter(
        models.ResourceType.id == resourcetype_id).first()
    result_dict = get_all_resource_type()
    result = s.execute(f"""
            select URR.user_id as user_id, URR.role_id, C.id as organization_id, PW.id as project_workspace_id, P.id as project_id
          from public."userresourcerole" URR
          left join public."organization" C on C.id = URR.resource_id
          left join public."projectworkspace" PW on PW.organization_id = C.id
          left join public."project" P on p.project_workspace_id  = PW.id
          where URR.user_id = {user_id} and URR.resource_type_id = {result_dict['Organization']}
          union all
          select URR.user_id as user_id, URR.role_id, null as organization_id, PW.id as project_workspace_id, P.id as project_id
          from public."userresourcerole" URR
          left join public."projectworkspace" PW on PW.id = URR.resource_id
          left join public."project" P on P.project_workspace_id = PW.id
          where URR.user_id = {user_id} and URR.resource_type_id = {result_dict['ProjectWorkspace']}
          union all
          select URR.user_id as user_id, URR.role_id, null as organization_id, null as project_workspace_id, P.id as project_id
          from public."userresourcerole" URR
          left join public."project" P on P.id = URR.resource_id
          where URR.user_id = {user_id} and URR.resource_type_id = {result_dict['Project']} ;
            """)
    for res in result:
        role_id = res['role_id']
        organization_id = res['organization_id']
        project_workspace_id = res['project_workspace_id']
        project_id = res['project_id']
        # question: does the role has permission to access the resource
        has_role_access = is_resource_accessable_for_role(req_resource.name,
                                                          role_id, operation)
        if not has_role_access:
            continue

        if req_resource.name == 'Organization':
            if organization_id == resource_id:
                return JSONResponse(content={"200": "Allowed"},
                                    status_code=200)
            # else:
            #     return JSONResponse(content={"401": "Unauthorized"},
            #                         status_code=401)

        elif req_resource.name == 'ProjectWorkspace':
            if project_workspace_id == resource_id:
                return JSONResponse(content={"200": "Allowed"},
                                    status_code=200)

            # else:
            #     return JSONResponse(content={"401": "Unauthorized"},
            #                         status_code=401)
        elif req_resource.name == 'Project':
            if project_id == resource_id:
                return JSONResponse(content={"200": "Allowed"},
                                    status_code=200)
            # else:
            #     return JSONResponse(content={"401": "Unauthorized"},
            #                         status_code=401)
        else:
            return JSONResponse(content={"400": "Bad Request"},
                                status_code=400)


def is_resource_accessable_for_role(resoure_name, role_id, operation):
    role = session.query(models.Role).filter(models.Role.id == role_id).first()
    accessable_resource = permission_dict[role.name]
    if resoure_name in accessable_resource.keys():
        allowed_op_list = accessable_resource[resoure_name]
        if operation in allowed_op_list:
            return True
    return False


