from app import logger
from app.api.services.base import db
from app.db.models import UserResourceRole
from app.db.schemas import ResourceRole, UpdateResourceRole


def create(user_resource_role: ResourceRole):
    try:
        users_resource = UserResourceRole()
        users_resource.user_id = user_resource_role.user_id
        users_resource.resource_id = user_resource_role.resource_id
        users_resource.resource_type_id = user_resource_role.resource_type_id
        users_resource.role_id = user_resource_role.role_id
        users_resource.save()
        logger.info("The UserResourceRole Added")
        return users_resource
    except Exception as e:
        logger.error(f'Details not Found')
        raise e


def get_user_resource_role():
    try:
        resource = db.query(UserResourceRole).all()
        logger.info("All details of UserResourceRole")
        return resource
    except Exception as e:
        logger.error(f'Details not found')
        raise e


def get_user_resource_roles(id: int):
    try:
        user_resource_role = db.query(UserResourceRole).filter_by(id=id).all()
        logger.info(f'The user_resource_role for the id : {id}')
        return user_resource_role
    except Exception as e:
        logger.error(f'There no details to return for the id : {id}')
        raise e


def delete_user_resource_roles(id: int):
    try:
        user_resource_role = db.query(UserResourceRole).filter_by(id=id)
        user_resource_role.delete()
        db.commit()
        logger.info(f'User Resource Role is deleted for the id: {id}')
        return f'User Resource Role is deleted for the id:{id}'
    except Exception as e:
        logger.error(f'There is no such a {id} to delete')
        raise e


def update_resources_role(id: int, usr_res_role: UpdateResourceRole):
    try:
        user_resource = db.query(UserResourceRole).filter_by(id=id).first()
        user_resource.user_id = usr_res_role.user_id
        user_resource.role_id = usr_res_role.role_id
        user_resource.resource_id = usr_res_role.resource_id
        user_resource.resource_type_id = usr_res_role.resource_type_id
        db.commit()
        return f'The User Resource details is update for the the id: {id}'
    except Exception as e:
        logger.error(f'There is no {id} to update the {usr_res_role}')
        raise e
