import time
from typing import List
from fastapi import APIRouter

from app.api.services.auth_service import role_decision
from app.api.services.user_resource_role import create, get_user_resource_role, \
    get_user_resource_roles, delete_user_resource_roles, update_resources_role
from app.db import get_db
from app.db.schemas import ResourceRole, UpdateResourceRole

router = APIRouter(
    prefix="/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)


@router.get('/')
def get_role_based_access(resource_id: int, resourcetype_id: int, user_id: int,
                          operation: str):
    return role_decision(resource_id, resourcetype_id, user_id, operation)


@router.post('/', tags=["auth"], response_model=ResourceRole)
def create_user_resource_role(user_resource_role: ResourceRole):
    user_resource_role = create(user_resource_role)
    return user_resource_role


@router.get('/get', response_model=List[ResourceRole])
def get_resources_role_view():
    return get_user_resource_role()


@router.get('/{id}', tags=['auth'], response_model=List[ResourceRole])
def get_resource_role_view(id: int):
    return get_user_resource_roles(id)


@router.delete('/{id}')
def delete_resource_role(id: int):
    return delete_user_resource_roles(id)


@router.put('/{id}')
def update_resource_role(id: int, usr_res_role: UpdateResourceRole):
    return update_resources_role(id, usr_res_role)


@router.post('/sample')
async def create_user():
    start = time.time()
    a = await my_func_1()
    b = await my_func_2()
    end = time.time()
    print('It took {} seconds to finish execution.'.format(round(end - start)))
    return {
        'a': a,
        'b': b
    }


@router.get('/sample2')
def user_create():
    start = time.time()
    a = my_func_1()
    b = my_func_2()
    end = time.time()
    print('It took {} seconds to finish execution.'.format(round(end - start)))
    return {
        'a': a,
        'b': b
    }
