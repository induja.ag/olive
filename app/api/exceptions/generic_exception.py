from typing import Any, Dict, Optional
from starlette.exceptions import HTTPException as StarletteHTTPException


class NotFoundException(Exception):
    def __init__(self, msg):
        self.msg = msg
        super().__init__(self.msg)


class BadRequestException(Exception):
    def __init__(self, msg):
        self.msg = msg
        super().__init__(self.msg)


class NotAcceptableException(Exception):
    def __init__(self, msg):
        self.msg = msg
        super().__init__(self.msg)


class InvalidRequestError(Exception):

    def __init__(self, msg):
        self.msg = msg
        super().__init__(self.msg)


class CustomHTTPException(StarletteHTTPException):

    def __init__(
            self,
            status_code: int,
            message: Any = None,
            headers: Optional[Dict[str, Any]] = None,
            type: str = None,
            details: Any = None,
    ) -> None:
        super().__init__(status_code=status_code, detail=details)
        self.headers = headers
        self.type = type
        self.message = message
        self.details = details
