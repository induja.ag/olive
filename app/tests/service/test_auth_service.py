import unittest
import pytest
from main import app as starter
from fastapi.testclient import TestClient
from app.api.services.auth_service import role_decision, \
    is_resource_accessable_for_role, get_all_resource_type


@pytest.fixture(autouse=True, scope="module")
def client():
    client = TestClient(starter)
    return client


def test_role_decission():
    user_id = 1
    resource_id = 1
    resourcetype_id = 1
    operation = 'read'
    response = role_decision(resource_id, resourcetype_id, user_id, operation)
    message = response
    print(message)
    assert response.status_code == 200 or 401


def test_is_resource_accessable_for_role_organization():
    resource_name = 'Organization'
    role_id = 1
    operation = 'read'
    response = is_resource_accessable_for_role(resource_name, role_id,
                                               operation)
    print(response)
    if response:
        assert response == True
    elif not response:
        assert response == False
    return response


def test_is_resource_accessable_for_role_projectworkspace():
    resource_name = 'ProjectWorkspace'
    role_id = 1
    operation = 'read'
    response = is_resource_accessable_for_role(resource_name, role_id,
                                               operation)
    print(response)
    if response:
        assert response == True
    elif not response:
        assert response == False
    return response


def test_is_resource_accessable_for_role_project():
    resource_name = 'Project'
    role_id = 1
    operation = 'read'
    response = is_resource_accessable_for_role(resource_name, role_id,
                                               operation)
    print(response)
    if response:
        assert response == True
    elif not response:
        assert response == False
    return response


def test_get_all_resource_type():
    response = get_all_resource_type()
    print(response)
    assert response


if __name__ == '__main__':
    unittest.main()
