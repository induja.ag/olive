import json
import unittest
import pytest
from fastapi.testclient import TestClient
from main import app


@pytest.fixture(autouse=True, scope="module")
def client():
    client = TestClient(app)
    return client


@pytest.mark.unit
class TestRoleBased:
    def test_role_based_access(self, client):
        response = client.get("/auth?resource_id=1&resourcetype_id=1&user_id=1"
                              "&operation=read")
        message = response.json()
        print(message)
        assert response.status_code == 200 or 401
        if response.status_code == 200:
            assert response.json() == {"200": "Allowed"}
        elif response.status_code == 401:
            assert response.json() == {"401": "Denied"}

    def test_create_user_resource_role(self, client):
        data = {"user_id": 1, "resource_id": 1,
                "resource_type_id": 1, "role_id": 1}
        response = client.post("/auth/", json.dumps(data))
        assert response.status_code == 200
        assert response.json() == {"user_id": 1, "resource_id": 1,
                                   "resource_type_id": 1, "role_id": 1}

    def test_get_user_resource_role(self, client):
        response = client.get("/auth/get")
        message = response.json()
        print(message)
        assert response.status_code == 200

    def test_get_user_resource_role_by_id(self, client):
        response = client.get("/auth/1")
        message = response.json()
        print(message)
        assert response.status_code == 200

    def test_delete_user_resource_role(self, client):
        response = client.delete("/auth/12")
        message = response.json()
        print(message)
        assert response.status_code == 200

    def test_update_user_resource_role(self, client):
        data = {"user_id": 1, "resource_id": 2,
                "resource_type_id": 1, "role_id": 1}
        response = client.put("auth/17", json.dumps(data))
        assert response.status_code == 200


if __name__ == '__main__':
    unittest.main()
