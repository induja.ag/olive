import json
import unittest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import pytest
from starlette.testclient import TestClient
from app.db import Base, get_db
from main import app
from unittest.mock import patch

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False,
                                   bind=engine)


def override_get_db():
    try:
        Base.metadata.create_all(bind=engine)
        Base.metadata.drop_all(bind=engine)
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()
        engine.dispose()


app.dependency_overrides[get_db] = override_get_db


@pytest.fixture(scope="module")
def client():
    client = TestClient(app)
    yield client


class TestUserResourceRole:

    def test_create_user(self, client):
        mock_get_patcher = patch('app.api.controller.service_api'
                                 '.create_user_resource_role')
        user_resource_role = {"user_id": 1,
                              "resource_id": 1,
                              "resource_type_id": 1,
                              "role_id": 1},
        mock_create = mock_get_patcher.start()
        response = client.post('/auth', json.dumps(user_resource_role))
        mock_create.return_value.status_code = 200
        print(response)
        mock_create.return_value.json.return_value = user_resource_role
        assert (response.status_code, 200)
        mock_get_patcher.stop()

    def test_get_user_resource_role(self, client):
        mock_get_patcher = patch('app.api.controller.service_api'
                                 '.get_resources_role_view')
        mock_get = mock_get_patcher.start()
        response = client.get("/auth/get")
        message = response.json()
        mock_get.return_value.status_code = 200
        print(message)
        assert (response.status_code, 200)
        mock_get_patcher.stop()

    def test_get_user_resource_role_by_id(self, client):
        mock_get_patcher = patch('app.api.controller.service_api'
                                 '.get_resource_role_view')
        mock_get = mock_get_patcher.start()
        response = client.get("/auth/6")
        mock_get.return_value.status_code = 200
        message = response.json()
        print(message)
        assert response.status_code == 200
        mock_get_patcher.stop()

    def test_delete_user_resource_role(self, client):
        mock_get_patcher = patch('app.api.controller.service_api'
                                 '.delete_resource_role')
        mock_delete = mock_get_patcher.start()
        response = client.delete("/auth/1")
        mock_delete.return_value.status_code = 200
        message = response.json()
        print(message)
        assert response.status_code == 200
        mock_get_patcher.stop()

    def test_update_user_resource_role(self, client):
        mock_get_patcher = patch('app.api.controller.service_api'
                                 '.update_resource_role')
        data = {"user_id": 1, "resource_id": 2,
                "resource_type_id": 1, "role_id": 1}
        mock_update = mock_get_patcher.start()
        response = client.put("auth/3", json.dumps(data))
        mock_update.return_value.status_code = 200
        message = response.json()
        print(message)
        assert response.status_code == 200
        mock_get_patcher.stop()


if __name__ == '__main__':
    unittest.main()
