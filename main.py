import uvicorn
from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi
from fastapi.openapi.docs import get_swagger_ui_html
import logging
from starlette.requests import Request
from starlette.responses import Response
from starlette.middleware.cors import CORSMiddleware
from starlette.middleware.base import (
    BaseHTTPMiddleware,
    RequestResponseEndpoint)
from app.core.factories import settings
from app.api.controller.service_api import router as auth_api

app = FastAPI()
app.include_router(auth_api)


@app.get("/docs", include_in_schema=False)
def overridden_swagger():
    return get_swagger_ui_html(
        openapi_url="/openapi.json",
        title="Olive Auth Service",
        swagger_favicon_url=" ")  # noqa


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Olive Auth Service",
        version="0.0.1",
        description=("<div font-size='10px'>Sequence flow for this"
                     "  microservice: <br /><br /> "),
        routes=app.routes,

    )
    openapi_schema["info"]["x-logo"] = {
        "url": ""  # noqa
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=9000, log_level="info")
